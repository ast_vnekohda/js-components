module.exports = {
    presets: [
        [
            '@babel/preset-env',
            {
                loose: true
            }
        ]
    ],
    plugins: [
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-transform-runtime'
    ]
};
