import Block from './common/Block'

export default class MyBlock extends Block {
    init () {
        super.init();
        this.event('click', this.handleClick)
    }
    handleClick () {
        this.emitter.emit('myblock.event', {test: 'test custom event'});
    }
};