import Block from './common/Block';
import eventMgr from '../common/eventMgr';

export default class testCmp extends Block {
    init() {
        super.init();
        this.event('click',  '.js-child-class', this.alert);
        this.onChild('myBlock', 'myblock.event', this.handleChild)
    }

	alert() {
		alert('alert');
	}

    handleChild (data) {
        alert(data.test);
    }

}
