import Component from '../Component';

/**
 * class representing Block component
 * @class Block
 * @example
 * <div data-cmp="block">This is Block cmp</div>
 */
export default class Block extends Component {
    render(htmlContent) {
        this.$el.html(htmlContent);
    }
}
