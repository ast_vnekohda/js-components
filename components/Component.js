/*eslint no-undefined: [0] */
import $ from 'jquery';
import eventMgr from '../common/eventMgr';

var $globalBody = $('body');

const HIDDEN_CLASS = 'd-none',
    noop = () => {},
    LOADED_CLASS = 'b-cmp__loaded',
    LOADING_CLASS = 'b-cmp___loading';

/**
 * @typedef IEvent
 * @property {string} [events.childID]
 * @property {Function} [events.childClass]
 * @property {string} events.eventName
 * @property {Function} events.fn
 */

/** General instance
 * @class Component
 */
export default class Component {

    /**
     * for correct work, must be redeclared at extended class (if default code level config required)
     *
     * @example
     *
     * <pre><code>
     * get configDefault() {
     *     return {
     *         'attr1' : 'val1',
     *         'attrN' : 'valN'
     *     };
     * }
     * </code></pre>
     */

    get configDefault() {
        return {};
    }

    /**
     * @description creates instance of Component
     * @param {JQuery} $el jquery component element
     * @param {Object} [config = {}] object with component custom configs
     * @param {string} config.cmpId component id
     * @param {string} config.id element id
     * @param {boolean} config.hidden detects if element should be hidden
     */

    constructor($el, config = {}) {
        /**
         * @type {JQuery}
         * @protected
         */
        this.$el = $el;
        this.$body = $globalBody;
        this.cmpName = $.trim($el.data('cmp'));

        this.$el.data('cmp-instance', this);

        this.config = this.getConfig($el, config);

        this.disposables = undefined;
        this.parentHandler = noop;

        this.items = [];

        this.emitter = {
            emit: (...args) => this.eventHandler(this, ...args),
            on: (...args) => this.onChild(this.id, ...args)
        };

        if (this.config.cmpId) {
            this.id = this.config.cmpId;
        } else {
            if (this.$el.attr('id')) {
                this.id = this.$el.attr('id');
            }
            if (!this.id && this.config.id) {
                this.id = this.config.id;
            }
        }

        if (this.$el.hasClass(HIDDEN_CLASS)) {
            this.config.hidden = true;
        }

        this.shown = !this.config.hidden;
    }

    getConfig(el, config = {}) {
        if (el instanceof jQuery) {
            el = el[0];
        }
        if (typeof el.getAttribute !== 'function') {
            return Object.assign({}, this.configDefault, config);
        }

        let jsonConfig = el.getAttribute('data-json-config');

        if (jsonConfig) {
            try {
                jsonConfig = JSON.parse(jsonConfig);
            } catch (error) {
                throw new Error(`Invalid json config for component ${el} ${error}`);
            }

            config = Object.assign(config, jsonConfig);
        }

        el.getAttributeNames().forEach(attrName => {
            if (typeof attrName === 'string' && attrName.includes('data-')) {
                var value = el.getAttribute(attrName);

                if (typeof value === 'string') {
                    config[this.camelCase(attrName.replace('data-', ''))] = this.getData(value);
                }
            }

        });

        return Object.assign({}, this.configDefault, config);
    }

    camelCase(string) {
        // Matches dashed string for camelizing
        var rmsPrefix = /^-ms-/,
            rdashAlpha = /-([a-z])/g;
        return string.replace(rmsPrefix, 'ms-').replace(rdashAlpha, (all, letter) => {
            return letter.toUpperCase();
        });
    }

    getData(data) {
        if (data === 'true') {
            return true;
        }

        if (data === 'false') {
            return false;
        }

        if (data === 'null') {
            return null;
        }

        // Only convert to a number if it doesn't change the string
        if (data === +data + '') {
            return +data;
        }
        var rbrace = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/;

        if (rbrace.test(data)) {
            try {
                return JSON.parse(data);
            } catch (e) {
                console.error(e); /* eslint-disable-line */
            }
        }

        return data;
    }

    onChild (childID, eventName, fn) {
        if (!this.childEventsID) {
            /**
             * @type {IEvent[]} events
             */
            this.childEventsID = [];
        }
        this.childEventsID.push({
            childID,
            eventName,
            fn
        });
    }
    onChilds (childComponentClass, eventName, fn) {
        if (!this.childEventsClass) {
            /**
             * @type {IEvent[]} events
             */
            this.childEventsClass = [];
        }
        this.childEventsClass.push({
            childClass: childComponentClass,
            eventName,
            fn
        });
    }
    eventHandler(childInstance, eventName, ...data) {
        if (this.childEventsID) {
            this.childEventsID.forEach(childEvent => {
                if (
                    childEvent.childID === childInstance.id &&
                    childEvent.eventName === eventName
                ) {
                    childEvent.fn.apply(this, data);
                }
            });
        }
        if (this.childEventsClass) {
            this.childEventsClass.forEach(childEventsClass => {
                if (
                    childInstance instanceof childEventsClass.childClass &&
                    childEventsClass.eventName === eventName
                ) {
                    childEventsClass.fn.apply(this, data);
                }
            });
        }
        if (this.parentHandler) {
            // @ts-ignore
            this.parentHandler(childInstance, eventName, ...data);
        }
    }

    init() {
        this.$el
            .addClass(LOADED_CLASS)
            .removeClass(LOADING_CLASS);
    }

    destroy() {
        if (this.disposables) {
            this.disposables.forEach(disposable => disposable());
            this.disposables = undefined;
        }

        for (var i = 0; i < this.items.length; ++i) {
            var item = this.items[i];
            if (item) {
                if (typeof item.destroy === 'function') {
                    item.destroy();
                }
            }
        }
        this.items = undefined;

        this.$el.data('cmp-instance', null);
        this.$el = undefined;
        this.childEventsID = undefined;
    }

    isBindedToDom() {
        var parent = this.$el[0];

        while (parent) {
            if (parent.tagName === 'HTML') {
                return true;
            }
            parent = parent.parentElement;
        }
        return false;
    }

    loadDependencies(dependencies) {
        if (!window.loadedLibs) {
            window.loadedLibs = {};
        }

        if ('css' in dependencies) {
            dependencies.css.map((url) => {
                if (!window.loadedLibs.url) {
                    $('head').append('<link rel="stylesheet" type="text/css" href="' + url + '">');
                    window.loadedLibs.url = url;
                }
            });
        }

        var d = $.Deferred(),
            jsDeps = dependencies.js.map((url) => {
                if (!window.loadedLibs.url) {
                    window.loadedLibs.url = $.getScript(url);
                }

                return window.loadedLibs.url;
            });

        $.when.apply($, jsDeps).done(() => {
            d.resolve();
        });

        return d;
    }

    /**
     * @description Assign action to event for DOM element
     * @param {string} eventName ex: 'click', 'change'
     * @param {string|Function} selector CSS/jQuery selector
     * @param {Function|jQuery} [cb] callback/jquery element
     * @param {jQuery|Function} [$element] callback/jquery element
     * @returns {Object} returns context
     */

    event(eventName, selector, cb, $element) {
        var self = this,
            fn = function () {
                return typeof cb === 'function'
                    ? cb.apply(self, [this].concat(Array.prototype.slice.call(arguments))) : '';
            };

        if (typeof selector === 'function' && typeof cb !== 'function') {
            $element = cb || self.$el;
            cb = selector;
            $element.on(eventName, fn);

            this.regDisposable(() => {
                if (fn) {
                    $element.off(eventName, fn);
                    fn = undefined;
                }
            });

        } else {
            $element = $element || self.$el;
            $element.on(eventName, selector, fn);

            this.regDisposable(() => {
                if (fn) {
                    $element.off(eventName, selector, fn);
                    fn = undefined;
                }
            });

        }
        return self;
    }

    /**
     * register disposable
     * @param {Function} fn calback
     */

    regDisposable(fn) {
        if (!this.disposables) {
            this.disposables = [];
        }
        this.disposables.push(fn);
    }

    /**
     * component event manager
     * @param {string} event event
     * @param {Function} handler event callback
     */

    eventMgr(event, handler) {
        handler = handler.bind(this);
        eventMgr.on(event, handler);

        function off() {
            if (handler) {
                eventMgr.off(event, handler);
                handler = undefined;
            }
        }

        this.regDisposable(off);

        return off;
    }

    /**
     * Invoke function for child component with passing args
     * @param {string} id of component
     * @param {string} name of function
     * @param {...any} args arguments
     */

    callFnForId(id, name, ...args) {
        return this.getById(id, cmp => cmp[name](...args)).then(([value]) => value);
    }

    /**
     *  Get All items inside component
     *
     * @returns All Items
     * @memberof Component
     */

    getAllItems() {
        return this.items;
    }

    /**
     * Search for child component instance which is returned in callback
     * @param {string} id of component
     * @param {Function} cb callback
     */

    getById(id, cb, results) {
        if (!id || !this.items) {
            return;
        }
        results = results || [];
        for (var c = 0; c < this.items.length; ++c) {
            let item = this.items[c];

            if (item.id === id) {
                results.push(cb.call(this, item));
            }
            item.getById(id, cb, results);
        }
        return Promise.all(results);
    }

    /**
     * @description Search for a single child component by specific condition
     * @param {Function} conditionCallback a function to be called on each item
     * Should return truely result if case component fits condition
     * @returns {Component} found component instance
     */
    findChildByCondition(conditionCallback) {
        for (var i = 0; i < this.items.length; i++) {
            const item = this.items[i];
            if (conditionCallback(item)) {
                return item;
            }

            const childResult = item.findChildByCondition(conditionCallback);
            if (childResult) {
                return childResult;
            }
        }
    }

    /**
     * @description Search for a single child component by it's prototype
     * @param {Function} prototype to search for
     * @returns {Component} found component instance
     */
    findChild(prototype) {
        return this.findChildByCondition(item => item instanceof prototype);
    }

    /**
     * @description Search for a single child component by it's "id"
     * @param {string} id to search for
     * @returns {Component} found component instance
     */
    findChildById(id) {
        return this.findChildByCondition(item => item.id === id);
    }

    /**
     * @description Search for child component with a given prototype and call a callback function
     * with the instance of found child component
     * @param {Function} prototype - prototype of the component to search for
     * @param {Function} callback - function to be called if chidl component with given prototype found
     */
    callOnChild(prototype, callback) {
        const componentInstance = this.findChild(prototype);
        if (componentInstance) {
            return callback(componentInstance);
        }
    }

    /**
     * @description Get all child items filtered by id
     * @param {string} id child component id
     * @returns {Component[]} an array of child components
     */
    getChildrenById(id) {
        if (!id || !this.items) {
            return;
        }

        return this.items.filter(item => item.id === id);
    }

    /**
     * Travels over nearest/next level child components
     * @param {Function} fn function for callback on each child
     * @returns {any[]} arrays of callback results
     */

    eachChild(fn) {
        return this.items.map((item) => {
            return fn(item);
        });
    }

    /**
     * Travels over all child components
     * @param {Function} fn function for callback on each child with any deep
     */

    eachChildDeep(fn) {
        for (var c = 0; c < this.items.length; ++c) {
            let item = this.items[c];

            if (item) {
                fn(item);
                item.eachChildDeep(fn);
            }
        }
    }
    hide() {
        if (this.shown) {
            this.$el.addClass(HIDDEN_CLASS);
            this.shown = false;
        }
    }
    show() {
        if (!this.shown) {
            this.$el.removeClass(HIDDEN_CLASS);
            this.shown = true;
        }
    }
    toggle(initialState) {
        let state = typeof initialState !== 'undefined' ? initialState : !this.shown;

        this[state ? 'show' : 'hide']();
    }
    isHidden() {
        return !this.shown;
    }
    isShown() {
        return this.shown;
    }
    callFnForEachId(id, name, ...args) {
        this.eachChildDeep((cmp) => {
            if (cmp.id === id) {
                cmp[name](...args);
            }
        });
    }
}
