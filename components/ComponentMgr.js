'use strict';

// eslint-disable-next-line no-undefined
if (Element.prototype.getAttributeNames === undefined) {
    Element.prototype.getAttributeNames = function() {
        var attributes = this.attributes;
        var length = attributes.length;
        var result = new Array(length);
        for (var i = 0; i < length; i++) {
            result[i] = attributes[i].name;
        }
        return result;
    };
}

import eventMgr from '../common/eventMgr';
import Component from './Component';
import $ from 'jquery';

var references = require('../components/componentsRegistry').default,
    emitter = eventMgr.getEmitter('componentmgr'),
    $document = $(document);


/**
 * @class RootComponent
 * @extends Component
 */
class RootComponent extends Component {}

var rootCmp = new RootComponent($document),
    RegistryMgr = {
        components: [],
        getComponent: function (name) {
            if (typeof this.components[name] === 'function') {
                return this.components[name]();
            }
            return Promise.resolve(this.components[name]);
        },
        getComponents: function () {
            return this.components;
        },
        setComponents: function (components) {
            this.components = components;
        }
    },
    inited = false;

/**
 * @description get layer of elements with attached components via data-cmp attribute
 * @param {HTMLElement} origElement origin node to start lookup of child components
 * @param {HTMLElement[]} result array of child components
 */
function getLayer(origElement, result = []) {
    var childrens = origElement.children;
    if (!childrens && origElement.isEqualNode(document)) {
        childrens = [document.documentElement];
    }
    if (childrens) {
        for (let i = 0; i < childrens.length; i++) {
            let child = childrens[i];
            if (child.getAttribute('data-cmp')) {
                result.push(child);
            } else {
                getLayer(child, result);
            }
        }
    }

    return result;
}

/**
 * @description set child instance to the parent items parameter
 * @param {HTMLElement} parentCmp origin node parent component
 * @param {Component} instance of child component
 * @returns {Promise} promise
 */
function pushInstanceToParent(parentCmp, instance) {
    parentCmp.items.push(instance);

    instance.parentHandler = parentCmp.eventHandler.bind(parentCmp);
    instance.init();
    return Promise.resolve();
}

function initComponent(domNode) {
    var $domNode = $(domNode),
        instance = $domNode.data('cmp-instance'),
        instancePromise;

    if (!instance) {
        let cmpName = $.trim($domNode.attr('data-cmp'));

        instancePromise = RegistryMgr.getComponent(cmpName).then((module) => {
            module = 'default' in module ? module.default : module;
            return new module($domNode);
        });
        $domNode.data('cmp-promise', instancePromise);
    } else {
        instancePromise = Promise.resolve(null);
    }

    /*eslint-disable-next-line no-use-before-define*/
    return Promise.all([instancePromise, initCmps(domNode)]).then(([instance]) => {
        if (!instance) {
            return;
        }

        let $parent = $domNode.parent().closest('[data-cmp]');

        if ($parent && $parent.data('cmp-promise')) {
            return $parent.data('cmp-promise').then((parentInstance) => {
                return pushInstanceToParent(parentInstance, instance);
            });
        }

        return pushInstanceToParent(rootCmp, instance);
    });
}

function initCmps(el = document) {
    if (el instanceof Component) {
        el = el.$el[0];
    }
    var initPromises = getLayer(el).map(domNode => initComponent(domNode));

    return Promise.all(initPromises);
}

function releaseCmps(baseCmp, force = false) {
    return new Promise(resolve => {
        let cmp = baseCmp || rootCmp, item, i;
        let autoResolve = true;

        if (!(cmp && cmp.items)) {
            return resolve('resolved');
        }

        for (i = cmp.items.length - 1; i >= 0; --i) {
            item = cmp.items[i];
            if (item) {
                autoResolve = false;
                if (
                    force ||
                    typeof item.isBindedToDom === 'function' &&
                    !item.isBindedToDom() &&
                    typeof item.destroy === 'function'
                ) {
                    item.destroy();
                    cmp.items.splice(i, 1);
                    resolve('resolved');
                } else {
                    releaseCmps(item, force).then(resolve);
                }
            }
        }

        if (autoResolve) {
            resolve('resolved');
        }
    });

}

function updateComponents(baseCmp, force = false) {
    emitter.emit('update.begin');
    if (inited) {
        return releaseCmps(baseCmp, force).then(() => {
            return initCmps(baseCmp).then(() => {
                emitter.emit('updated');
            });
        }).catch((error) => {
            window.console.log('Error update components: ' + error);
            return Promise.reject(error);
        });
    }

    return Promise.resolve('updated');
}

eventMgr.registerAction('componentmgr.update', updateComponents);
eventMgr.registerAction('componentmgr.update.forced', (baseCmp) => updateComponents(baseCmp, true));
eventMgr.registerAction('componentmgr.release', releaseCmps);
eventMgr.registerAction('componentmgr.initcmp', initCmps);
eventMgr.registerAction('componentmgr.getRoot', () => new Promise(resolve => resolve(rootCmp)));


$document.ajaxStop(() => {
    setTimeout(updateComponents, 10);
});

function ComponentMgr(overridenReferences) {
    RegistryMgr.setComponents(overridenReferences || references);
    this.definitions = references;
    initCmps(rootCmp).then(() => {
        inited = true;
    }).catch((error) => {
        window.console.log('Error update components: ' + error);
        return Promise.reject(error);
    });

    return rootCmp;
}

let componentMgr = new ComponentMgr();

export default componentMgr;

if (module.hot) {
    module.hot.accept('../components/componentsRegistry', () => {

        releaseCmps(rootCmp, true).then(() => {
            references = require('../components/componentsRegistry').default;
            RegistryMgr.setComponents(references);
            rootCmp = new RootComponent($document);
            initCmps(rootCmp).then(() => {
                inited = true;
                window.console.info('Cmps reloaded');
            });
        });
    });
}
