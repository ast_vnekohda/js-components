const webpack = require('webpack');


module.exports = function () {
	return {
	  mode: 'development',
	  entry: ['./global.js', './components/TestCmp.js', './components/MyBlock.js'],
	  output: {
		  filename: 'main.js'
	  },
	  devtool: 'source-map',
	  plugins: [
		    new webpack.ProvidePlugin({
		        $: 'jquery',
		        jQuery: 'jquery'
		    })
	  ],
	  module: {
		  rules: [
	        {
	            test: /\.js$/,
	            exclude: /(node_modules|bower_components)/,
	            use: {
	                loader: 'babel-loader'
	            }
	        }
		  ]
	  },
	  optimization: {
		  minimize: false
	  }
  }
};
/*
  module: {
    rules: [
      { test: /\.css$/, use: 'css-loader' },
      { test: /\.ts$/, use: 'ts-loader' }
    ]
  }
 */
